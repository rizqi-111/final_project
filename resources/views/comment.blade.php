@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="{{ asset('upload/1603468626Untitled.mp4') }}" allowfullscreen></iframe>
        </div>
        </div>
        <div class="col-md-4">
            <comment-box-component />
        </div>
    </div>
</div>
@endsection
