@extends('layouts.app')

@section('content')
<div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <admin-component/>
            <div>
                <router-view></router-view>
            </div>
        </div>
    </div>
</div>
@endsection