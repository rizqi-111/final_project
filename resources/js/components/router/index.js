import Event from '../Admin/EventTableComponent'
import AddEvent from '../Admin/AddEventComponent'
import Order from '../Admin/OrderTableComponent'

import UserEvent from '../User/EventTableComponent'
import UserOrder from '../User/OrderTableComponent'
import UserStreaming from '../User/StreamingTableComponent'

export default{
    mode:'history',
    routes: [
        //route admin
        {
            path: '/admin/dashboard',
            name: 'dashboard',
            component: Event
        },
        {
            path: '/admin/AddEvent',
            name: 'addEvent',
            component: AddEvent
        },
        {
            path: '/admin/order',
            name: 'order',
            component: Order
        },

        //route user
        {
            path: '/user/dashboard',
            name: 'userdashboard',
            component: UserEvent
        },
        {
            path: '/user/order',
            name: 'userorder',
            component: UserOrder
        },
        {
            path: '/user/streaming',
            name: 'userstreaming',
            component: UserStreaming
        },
    ]
}