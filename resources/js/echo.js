import BusEvent from './bus'

Echo.join('comment-channel')
    .here((users) => {
        BusEvent.$emit('comment.here', users)
    })
    .joining((user) => {
        BusEvent.$emit('comment.joining', user)
    })
    .leaving((user) => {
        BusEvent.$emit('comment.leaving', user)
    })
    .listen('CommentStoredEvent', (e) => {
        console.log(e);
        BusEvent.$emit('comment.sent', e.data);
    });