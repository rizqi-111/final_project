<?php

use Illuminate\Database\Seeder;

class RoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('routes')->insert([
        	'role_id' => '0',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'user'
        ]);

        DB::table('routes')->insert([
        	'role_id' => '0',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'payment-user'
        ]);

        DB::table('routes')->insert([
            'role_id' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'all-order'
        ]);

        DB::table('routes')->insert([
            'role_id' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'all-event'
        ]);

        DB::table('routes')->insert([
            'role_id' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'all-comment'
        ]);

        DB::table('routes')->insert([
            'role_id' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'new-comment'
        ]);

        DB::table('routes')->insert([
            'role_id' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'video'
        ]);

        DB::table('routes')->insert([
            'role_id' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'make-order'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'admin'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'user'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'admin-event'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'admin-info'
        ]);

        DB::table('routes')->insert([
            'role_id' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'user-info'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'payment-admin'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'all-order'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'all-event'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'all-comment'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'new-comment'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'video'
        ]);

        DB::table('routes')->insert([
            'role_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        	'name' => 'event-store'
        ]);
    }
}
