<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Event;
use App\Http\Resources\Payment as PaymentResource;
use GuzzleHttp\Client;

class PaymentController extends Controller
{
    //
    public function getByUser($id){
        $payment = Payment::whereHas('order' , function($query) use ($id) {
            $query->where('user_id',$id);
        });
        
        return PaymentResource::collection($payment->get());
    }

    public function getByAdmin($id){
        $event = Event::where('admin_id',$id)->get();
        $event_id = [];
        foreach($event as $e){
            array_push($event_id,$e->id);
        }

        $payment = Payment::whereHas('order' , function($query) use ($event_id) {
            $query->whereIn('event_id',$event_id);
        });
        
        return PaymentResource::collection($payment->get());
    }

    public function store($price,$order_id,$method){
        $payment = Payment::create([
           'amount' => $price,
           'method' => $method,
           'order_id' => $order_id
        ]);

        $response_midtrans = $this->midtrans_store($payment);

        return response()->json([
           'response_code' => '00',
           'response_msg' => 'success',
           'data' => $response_midtrans
        ]);
   }

   protected function midtrans_store(Payment $payment){
       $server_key = base64_encode(config('app.midtrans.server_key'));
       $base_uri = config('app.midtrans.base_uri');
       $client = new Client([
           'base_uri' => $base_uri
       ]);

       $headers = [
           'Accept' => 'application/json',
           'Authorization' => 'Basic ' . $server_key,
           'Content-Type' => 'application/json'
       ];

       switch($payment->method){
           case 'bca' :
               $body = [
                   "payment_type" => "bank_transfer",
                   "transaction_details" => [
                       "order_id" => $payment->order_id,
                       "gross_amount" => $payment->amount
                   ],
                   "bank_transfer" => [
                       "bank" => "bca"
                   ]
               ];
               break;
           case 'permata' :
               $body = [
                   "payment_type" => "permata",
                   "transaction_details" => [
                       "order_id" => $payment->order_id,
                       "gross_amount" => $payment->amount
                   ]
               ];
           break;

           default: $body = []; break;
       }

       $res = $client->post('/v2/charge',[
           'headers' => $headers,
           'body' => json_encode($body)
       ]);

       return json_decode($res->getBody());
   }
}
