<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Events\CommentStoredEvent;
use App\Http\Resources\Comment as CommentResource;

class CommentController extends Controller
{
    public function all_comments(){
        $comments = Comment::with('user')->orderBy('created_at','desc')->take(10)->get();
        return CommentResource::collection($comments);
    }

    public function store(Request $request){
        //dd(auth()->user()->id);
        $comment = Comment::create([
            'subject' => $request->subject,
            'user_id' => auth()->user()->id,
            'event_id' => ''
        ]);

        broadcast(new CommentStoredEvent($comment))->toOthers();

        return $comment;
    }

}
