<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Http\Resources\Order as OrderResource;
use App\Event;
use App\Http\Controllers\PaymentController as PaymentController;

class OrderController extends Controller
{
    //
    public function index(){
        return view('order/index');
    }

    public function getAll(){
        $order = Order::all();
        return OrderResource::collection($order);
    }

    public function store(Request $request,$event_id,$user_id){
        $order = Order::create([
            'user_id' => $user_id,
            'event_id' => $event_id
        ]);
        
        $event = Event::find($event_id);

        $payment = app('App\Http\Controllers\PaymentController')->store($event->price,$order->id,$request->method);    

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'success',
            'data' => $order,
            'payment' => $payment
         ]);
    }
}