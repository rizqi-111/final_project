<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Http\Resources\Event as EventResource;
use Auth;

class EventController extends Controller
{
    //
    public function index(){
        return view('event/index');
    }

    public function getAdmin()
    {
        $admin = auth()->user();
        
        return response()->json([
            'response_code' => '00',
            'response_msg' => 'success',
            'admin' => $admin
        ]);
    }

    public function getAll(){
        $event = Event::all();
        return EventResource::collection($event);
    }

    public function getByAdmin($id){
        $event = Event::where('admin_id',$id)->get();
        return EventResource::collection($event);
    }

    public function getVideo($event_id){
        $event = Event::find($event_id)->get();
        return EventResource::collection($event);
    }

    public function store(Request $request){
        $validate = $request->validate([
            'name' => 'required|min:4',
            'descrption' => 'required|min:4',
            'price' => 'required',
            'video' => 'required',
            'banner' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
        ]);

        $upload_path = public_path('upload');
        $fileNameBanner = time() . $request->banner->getClientOriginalName();
        $request->banner->move($upload_path, $fileNameBanner);

        $upload_path = public_path('upload');
        $fileNameVideo = time() . $request->video->getClientOriginalName();
        $request->video->move($upload_path, $fileNameVideo);
        
        $event = Event::create([
            'name' => $request->name,
            'descrption' => $request->descrption,
            'price' => $request->price,
            'banner' => $fileNameBanner,
            'video_source' => $fileNameVideo,
            'time_start' => $request->time_start,
            'time_end' => $request->time_end,
            'admin_id' => $request->admin_id
        ]);
        
        return response()->json([
            'response_code' => '00',
            'response_msg' => 'success',
            'data' => $event
         ]);
    }
}
