<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class Event extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->descrption,
            'banner' => $this->banner,
            'price' => $this->price,
            'video_source' => $this->video_source,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end,
            'admin' => User::find($this->admin_id)
        ];
    }
}
