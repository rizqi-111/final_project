<?php

namespace App\Http\Middleware;

use Closure;
use App\Route;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(\Route::current()->getName());
        $route_name = \Route::current()->getName();
        $user = \Auth::user();

        if($this->check_route($route_name)){
            return $next($request);
        }

        return abort(403);
    }

    public function check_route($route_name){
        $role_id = Route::where('name', $route_name)->first()->role_id;
        $role = \Auth::user()->role;
        $check = false;

        if($role == $role_id){
            $check = true;
        }

        return $check;
    }
}
