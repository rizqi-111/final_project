<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Order extends Model
{
    //
    use Uuid;

    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function event(){
        return $this->belongsTo('App\Event');
    }

    public function payment(){
        return $this->hasOne('App\Payment');
    }
}
