<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Payment extends Model
{
    //
    use Uuid;

    protected $guarded = [];

    public function order(){
        return $this->belongsTo('App\Order');
    }
}
