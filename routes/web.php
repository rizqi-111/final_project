<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'role','log.route'])->group(function () {
    Route::view('/user/{any?}','user')->name('user');
    Route::view('/admin/{any?}','admin')->name('admin');
    Route::get('/infoAdmin','EventController@getAdmin')->name('admin-info');
    Route::get('/infoUser','EventController@getAdmin')->name('user-info');
    Route::get('/event/getByAdmin/{id}','EventController@getByAdmin')->name('admin-event');
    Route::get('/order/getAll','OrderController@getAll')->name('all-order');
    Route::get('/payment/getByUser/{id}','PaymentController@getByUser')->name('payment-user');
    Route::get('/payment/getByAdmin/{id}','PaymentController@getByAdmin')->name('payment-admin');
    Route::get('/comments','CommentController@all_comments')->name('all-comment');
    Route::post('/comments','CommentController@store')->name('new-comment');
    Route::get('/event/getAll','EventController@getAll')->name('all-event');
    Route::get('/event/getVideo/{id}','EventController@getVideo')->name('video');

    Route::post('/order/make/{event_id}/{user_id}','OrderController@store')->name('make-order');
    Route::post('/event/store','EventController@store')->name('event-store');
});

Route::get('/log','LogController@getAll');
Route::get('/log/{fileName}',['as' => 'log-files.show','uses' => 'LogController@show']);